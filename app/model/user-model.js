const sessionDB = require('../../config/database').session;
const Promise = require('bluebird');

const findByNameAndPassword = (email, password) => {
  return new Promise ((resolve, reject) => {

      sessionDB
        .run('MATCH (n:User {email:{emailParam} }) RETURN n', {emailParam: email})
        .then((result) => {
          if(result.records.length === 0) {
            resolve(null, false);
          }
          if (result.records[0]._fields[0].properties.password !== password) {
            resolve(null, false);
          } else {
            resolve(result.records[0]._fields[0].properties);
          }
        })
        .catch((err) => {
          reject(err);
        });

  });
};

const findById = (id) => {
  return new Promise ((resolve, reject) => {
    sessionDB
      .run('MATCH (n:User {id: {idParam}}) RETURN n', {idParam: id})
      .then((result) => {
        resolve(result.records[0]._fields[0].properties);
      })
      .catch((err) => {
        reject(err);
      });

  });
};

const addUser = (user) => {
  return new Promise((resolve, reject) => {

    sessionDB
      .run('CREATE(n:User {id: {idParam}, firstname:{firstnameParam}, lastname: {lastnameParam}, email: {emailParam}, password: {passwordParam} }) RETURN n',
      {idParam: user.uid, firstnameParam: user.firstname, lastnameParam: user.lastname, emailParam: user.email, passwordParam: user.password})
      .then((result) => {
        resolve(result.records[0]._fields[0].properties);
      })
      .catch((err) => {
        reject(err);
      });

  });
};

const addFriendToUser = (idCurrentUser, idFriendToAdd) => {
  return new Promise((resolve, reject) => {

    sessionDB
    .run('MATCH (a:User {id: {idUser}}), (b:User {id: {idUserFriend}}) MERGE(a)-[r:FRIEND_WITH]-(b) RETURN a,b', {idUser: idCurrentUser, idUserFriend: idFriendToAdd})
    .then((results) => {
      resolve(true);
    })
    .catch((err) => {
      reject(err);
    });
  });
};

const listUsers = (idCurrentUser) => {
  return new Promise((resolve, reject) => {
    let users = [];
    sessionDB
    .run('MATCH (a:User) where a.id <> {idUser} RETURN a', {idUser: idCurrentUser})
    .then((results) => {
       results.records.forEach((result) => {
        users.push({firstname: result._fields[0].properties.firstname,
                      lastname: result._fields[0].properties.lastname,
                      id: result._fields[0].properties.id
                    });
      });
      resolve(users);
    })
    .catch((err) => {
      reject(err);
    });
  });
};

const listFriends = (id) => {
  return new Promise((resolve, reject) => {

    sessionDB
    .run('MATCH (a:User {id: {idUser}})-[r:FRIEND_WITH]->(friends) RETURN friends', {idUser: id})
    .then((results) => {
      let friends = [];
      results.records.forEach((result) => {
        friends.push({firstname: result._fields[0].properties.firstname,
                      lastname: result._fields[0].properties.lastname,
                      id: result._fields[0].properties.id
                    });
      });
      resolve(friends);
    })
    .catch((err) => {
      reject(err);
    });
  });
};

module.exports = {
  findByNameAndPassword: findByNameAndPassword,
  findById: findById,
  addUser: addUser,
  addFriendToUser: addFriendToUser,
  listFriends: listFriends,
  listUsers: listUsers
};
