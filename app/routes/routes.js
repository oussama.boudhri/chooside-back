const jwt      = require('jwt-simple');
const config  = require('../../config/database');

module.exports = (app, passport) => {

  app.post('/login', passport.authenticate('local-login', {failureFlash : true }), (req, res) => {
    if (req.user){
      let token = jwt.encode(req.user, config.secret);
      res.json({ userIsAuthenticated: true, token: 'JWT ' + token });
    }
  });

  app.post('/signup', passport.authenticate('local-signup', {failureFlash : true }), (req, res) => {
    res.json({ userRegistered: true });
  });

  app.get('/logout', (req, res) => {
   req.logout();
  });

};
