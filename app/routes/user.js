const UserModel = require('../model/user-model');
const jwt       = require('jwt-simple');
const config    = require('../../config/database');

module.exports = (app, passport) => {

  app.get('/api/profile', passport.authenticate('jwt', { session: false}), (req, res) => {

    let token = getToken(req.headers);
    if (token) {
      let decoded = jwt.decode(token, config.secret);

      UserModel.findById(decoded.id).then((userFound) => {
        if (!userFound) {
          return res.status(403).send({success: false, msg: 'Authentication failed, User not found'});
        } else {
          return res.json(userFound);
        }
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }
  });

  app.get('/api/listfriends', passport.authenticate('jwt', { session: false}), (req, res) => {

    let token = getToken(req.headers);
    if (token) {
      let decoded = jwt.decode(token, config.secret);

      UserModel.listFriends(decoded.id).then((friends) => {
          return res.json(friends);
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }

  });

    app.get('/api/users', passport.authenticate('jwt', { session: false}), (req, res) => {

    let token = getToken(req.headers);
    if (token) {
      let decoded = jwt.decode(token, config.secret);

      UserModel.listUsers(decoded.id).then((users) => {
          return res.json(users);
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }

  });

  app.get('/api/addfriend', passport.authenticate('jwt', { session: false}), (req, res) => {
    let idUserToAdd = req.query.id;

    let token = getToken(req.headers);
    if (token) {
      let decoded = jwt.decode(token, config.secret);

      UserModel.addFriendToUser(decoded.id, idUserToAdd).then((relationAdded) => {
        if (relationAdded) {
          return res.send({success: true, msg: 'Relation added successfully'});
        } else {
          return res.status(403).send({success: false, msg: 'Fail when adding relation'});
        }
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }

  });

  const getToken = (headers) => {
    if (headers && headers.authorization) {
      let parted = headers.authorization.split(" ");
      if (parted.length === 2){
        return parted[1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  };

};
