const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver('bolt://neo4j:7687', neo4j.auth.basic('neo4j', 'root'));
const session = driver.session();

module.exports = {
  session: session,
  secret: "MYSECRETKEY"
};
