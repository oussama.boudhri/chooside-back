const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const userModel =  require('../app/model/user-model');
const uuid = require('node-uuid');
const config = require('./database');

module.exports = (passport) => {

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
      userModel.findById(id).then((result) => {
        return done(null, result);
      });
    });

    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    },
    (req, email, password, done) => {
        if (email)
            email = email.toLowerCase();
            userModel.findByNameAndPassword(email, password).then((result) => {
              return done(null, result);
            });

    }));

    passport.use('local-signup', new LocalStrategy({
          usernameField : 'email',
          passwordField : 'password',
          passReqToCallback : true
      },
      (req, email, password, done) => {

        let user = {
          firstname: req.query.firstname,
          lastname: req.query.lastname,
          password: req.query.password,
          email: req.query.email
        };

        let uid = uuid.v4();
        user.uid = uid;

        userModel.addUser(user).then((result) => {
          return done(null, result);
        });
      }));

      let opts= {};
      opts.secretOrKey = config.secret;
      opts.jwtFromRequest = ExtractJwt.fromAuthHeader();

      passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        userModel.findById(jwt_payload.id).then((user) => {
          if(!user) {
            return done(null, false);
          } else {
            return done(null, user);
          }
        });
      }));

};
